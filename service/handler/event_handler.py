from datetime import datetime

from service.dao.events import store_event_into_db, store_error_log_into_db, store_log_into_db


def validate_event_data():
    return True



def handle_event(event_data):
    event_data = event_data
    event_triggered_date = event_data.get("eventTriggeredDime", datetime.now())
    event_triggered_epoch = event_data.get("eventTriggeredEpoch", int(datetime.now().strftime("%s")))

    if not validate_event_data():
        raise Exception

    insertion_id = None
    if event_data["identity"] == "ERROR_LOG":
        insertion_id = store_error_log_into_db(event_data,
                                               event_triggered_date,
                                               event_triggered_epoch)

    elif event_data["identity"] == "LOG":
        insertion_id = store_log_into_db(event_data,
                                         event_triggered_date,
                                         event_triggered_epoch)

    elif event_data["identity"] == "ANALYTICS_EVENT":
        insertion_id = store_event_into_db(event_data,
                                           event_triggered_date,
                                           event_triggered_epoch)
    return insertion_id
