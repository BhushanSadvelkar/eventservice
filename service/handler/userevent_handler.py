
from service.dao.userevents import store_user_event_into_db
from service.utils.userevent import is_valid_event, get_event_structure
from service.utils.kafka_event_producer import source_user_event



def handle_user_event(event_data):
    status, message = is_valid_event( event_data )
    
    if not status:
        return message, False
    
    event_structure = get_event_structure(event_data)
    id = store_user_event_into_db( event_structure )
    
    if event_structure["userId"]:
        source_user_event(
            "UI",
            event_structure["userId"],
            event_structure["eventName"],
            event_structure["eventProperties"]
            )
    
    return id, True