from flask.globals import request
from service.handler.userevent_handler import handle_user_event
from service.utils.resource import BaseResource
from service.utils.response import ok_response, error_response


class UserEvent(BaseResource):
    def post(self):
        event_data = request.get_json(force=True)
        
        resp, status = handle_user_event(event_data)
        
        if not status:
            return error_response(400, resp)
        
        response = {
            "id": resp,
            "message":"Event has been recorded."
        }
        
        return ok_response(response)

