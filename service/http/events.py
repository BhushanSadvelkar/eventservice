from flask.globals import request
from service.handler.event_handler import handle_event
from service.utils.resource import BaseResource
from service.utils.response import ok_response


class Events(BaseResource):
    def post(self):
        event_data = request.get_json(force=True)

        id = handle_event(event_data)
        response = {
            "id": "Event has been recorded with id {}".format(id),
        }

        return ok_response(response)


