import django
django.setup()


from rq.decorators import job
from config.settings import redis_conn, config
from kafka import KafkaProducer
from json import dumps


@job('api_logs', connection=redis_conn, timeout=-1)
def create_and_send_logs(start_time, end_time, status, headers, params, request_method, path, error_trace=None):
    if request_method != 'OPTIONS':
        try:
            total_time = str(end_time - start_time)
            log = {
                "path": path,
                'headers': dict(headers),
                'request_method': request_method,
                'error_trace': str(error_trace) if error_trace else '',
                'total_time': str(total_time),
                "start_time": str(start_time),
                "status": status,
                'params':params

            }
        except Exception as e:
            print ('logger exception === >', e)
        send_log(log)


def send_log(log):
    try:
        kafka_servers = config["KAFKA_BROKER"]
        producer = KafkaProducer(bootstrap_servers=[kafka_servers],
                                 value_serializer=lambda x:
                                 dumps(x).encode('utf-8'))
        result = producer.send('api_logs', value=log)
        result.get(timeout=60)
    except Exception as e:
        print 'Failed to send kafka log', e
