headers_mapping = {'csv': {'content-type': 'application/csv'},
                   'json': {'content-type': 'application/json'}}


def ok_response(response, headers='json', http_status=200):
    return response, http_status, headers_mapping[headers]


def error_response(http_status, errors, headers='json'):
    return errors, http_status, headers_mapping[headers]


def redirect_response(redirection_url, http_status=301):
    return redirection_url, http_status, {}
