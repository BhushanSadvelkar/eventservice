import email.utils
import smtplib
from config.settings import config
from rq.decorators import job
from config.settings import redis_conn
from io import BytesIO
import pandas
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

USERNAME_SMTP = config["SMTP"]['USERNAME']

PASSWORD_SMTP = config["SMTP"]['PASSWORD']

HOST = config['HOST']
PORT = config['PORT']


def msg_attachment(filename, path):
    attachment = MIMEApplication(open(path, 'rb').read())
    attachment.add_header('Content-Disposition', 'attachment', filename=filename)
    return attachment


@job('mails', connection=redis_conn, timeout=-1)
def send_mail(sender_mail, sender_name, receiver_list, subject=None, content=None, attachments=None, list_of_dict=None):
    receivers = ', '.join(receiver_list)
    msg = MIMEMultipart()
    msg['subject'] = subject
    msg['from'] = email.utils.formataddr((sender_name, sender_mail))
    msg['to'] = receivers

    if content:
        text = MIMEText(content, 'html')
        msg.attach(text)

    if attachments:
        for attachment in attachments:
            msg.attach(msg_attachment(attachment['filename'], attachment['path']))

    if list_of_dict:
        csv_buffer = BytesIO()
        csv_df = pandas.DataFrame(list_of_dict)
        csv_df.to_csv(csv_buffer, index=False)
        attachment = MIMEApplication(csv_buffer.getvalue())
        attachment['Content-Disposition'] = 'attachment; filename="export.csv"'
        msg.attach(attachment)

    try:
        server = smtplib.SMTP(HOST, PORT)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(USERNAME_SMTP, PASSWORD_SMTP)
        server.sendmail(sender_mail, receiver_list, msg.as_string())
        server.close()
    except Exception as e:
        print ("Error: ", e)
    else:
        print ("Email sent!")


