from datetime import datetime

def get_event_structure(event_data):
    return {
        "eventTriggeredDate":datetime.now(),
        "eventTriggeredEpoch":datetime.now().strftime("%s"),
        "eventName":event_data["eventName"],
        "eventPlatform":event_data["platform"],
        "eventProject":event_data["project"],
        "eventProperties":event_data["properties"],
        "userId":event_data["userId"] 
    }

def is_valid_platform(platform):
    return platform.upper() in ["APP","WEB"]


def is_valid_project(project):
    return project.upper() in ["B2C","B2B"]

def is_valid_event(event_data):
    
    platform = event_data.get("platform", "")
    project = event_data.get("project", "")
    
    if is_valid_project(project) and is_valid_platform(platform):
        return True,"Valid Data"    
    
    if not is_valid_platform(platform):
        return False,"Invalid Platform"
    
    if not is_valid_project(project):
        return False, "Invalid Project"    