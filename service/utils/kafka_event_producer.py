import json
from kafka import KafkaProducer

from config.settings import config

kafka_producer_ip = config["KAFKA_BROKER"]
kafka_producer = KafkaProducer(bootstrap_servers='{}'.format(kafka_producer_ip))

def push_data_to_kafka(topic, payload):
    kafka_producer.send(
        topic,
        value=json.dumps(payload)
        )
    kafka_producer.flush()
  
  
def source_user_event(entity_type, entity_id, event_name, meta):
    push_data_to_kafka(
        "ui_user_events",
        {
            "entityId": entity_id,
            "entityType": entity_type,
            "eventName": event_name,
            "meta": meta
            }
    )