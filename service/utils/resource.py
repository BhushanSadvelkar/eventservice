import os
import json
from flask import redirect
from flask import request, session, current_app as app
import flask_restful as restful
from datetime import datetime
from decimal import Decimal
from time import time
from os.path import join
import traceback
from datetime import timedelta
from django import db
from functools import wraps
import csv


from config.settings import config
from service.utils.send_logs_to_kafka import create_and_send_logs
from service.utils.email_sender import send_mail


def is_2XX(code):
    return code / 100 == 2


def sanitize_response(response):
    http_status = 200
    data = None
    headers = {}
    if isinstance(response, tuple):
        (data, http_status, headers) = response
    else:
        data = response
    data = sanitize_date(data)
    if is_2XX(http_status) and isinstance(data, list):
        data = dict(
            results=data,
            count=len(data),
            hasPrev=False,
            hasNext=False
        )

    return (data, http_status, headers)


def sanitize_date(data):
    if isinstance(data, list):
        data = [sanitize_date(x) for x in data]
    elif isinstance(data, dict):
        data = python_dict_to_json(data)
    return data


def python_dict_to_json(data):
    for k, v in data.items():
        if isinstance(v, datetime):
            if request.headers.get("Client-Code") == "charmander":
                v = v + timedelta(hours=5, minutes=30)
            data[k] = datetime.strftime(v, "%Y-%m-%dT%H:%M:%SZ")

        elif v == None and (not "storeauxiliary" in request.environ["PATH_INFO"]):
            data[k] = ""
        elif isinstance(v, Decimal):
            data[k] = float(v)
        else:
            data[k] = sanitize_date(v)
            if k == 'coupon':
                if data[k]['couponApplied'] == "":
                    data[k]['couponApplied'] = None
            if k == 'deliveryDetails' and 'order' in request.environ["PATH_INFO"]:
                if data['deliveryDetails'] == "":
                    data['deliveryDetails'] = None

    return data


def format_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        data, http_status, headers = sanitize_response(func(*args, **kwargs))
        return (data, http_status, headers)

    return wrapper


def handle_redirects(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        data, http_status, headers = sanitize_response(func(*args, **kwargs))
        if http_status in [301, 302]:
            return redirect(data, code=http_status)
        else:
            return (data, http_status, headers)

    return wrapper


def create_local_file(file_storage, prefix=None):
    location = "/var/www/html"

    epoch = int(time() * 1000)

    file_name = "{}-{}".format(epoch, file_storage.filename)

    if prefix:
        file_name = "{}-{}".format(prefix, file_name)

    file_name = file_name.replace(" ", "_")
    url = "{}{}".format(config['SELF_URL'], file_name)
    file_location = join(location, file_name)
    file_storage.save(file_location)
    return url


def cors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            data, status, headers = func(*args, **kwargs)

            cors_allow_headers = ', '.join(app.config.get('CORS_ALLOW_HEADERS',
                                                          ["*", "authorization", "client-code", 'Content-Type',
                                                           'Client-Code', 'Accept-Language', 'Authorization']))
            cors_allow_origins = ', '.join(app.config.get('CORS_ALLOW_ORIGINS', ["*"]))
            cors_allow_methods = ', '.join(app.config.get('CORS_ALLOW_METHODS', ["*"]))

            headers.update({
                'Access-Control-Allow-Headers': cors_allow_headers,
                'Access-Control-Allow-Origin': cors_allow_origins,
                'Access-Control-Allow-Methods': cors_allow_methods
            })

            return (data, status, headers)
        except Exception as e:
            traceback.print_exc()
            send_exception_email(e)
        finally:
            db.connections.close_all()

    return wrapper


def send_exception_email(e):
    content = ""

    content = content + "<b>PATH : </b> {}".format(request.environ["PATH_INFO"])
    content = content + "<br>"
    content = content + "<br>"

    params = request.args.to_dict()
    content = content + "<b>Request Params : </b> {} <br><br><br>".format(json.dumps(params))

    headers = request.headers
    content = content + "<b>Headers</b><br>"
    for header in headers:
        content = content + "{}<br>".format(str(header))

    content = content + "<br> SERVER_IP : {} </br>".format(os.environ.get("SERVER_IP", ""))
    content = content + "<br><br><br>"
    content = content + "<b> Traceback </b>"
    for t in traceback.format_exc().split("\n"):
        left_pad = 0
        for c in t:
            if c == ' ':
                left_pad += 10
            else:
                break

        content = content + "<p style=\"padding-left:{}px\" >{}</p>".format(left_pad, t)
    if not os.getenv('DISABLE_500_MAIL'):
        send_mail(
            content=content,
            sender_mail="ooops@mpaani.com",
            sender_name="mPaani",
            receiver_list=["technology@mpaani.com"],
            subject="ONLINE Delivery Exception : {}".format(str(e))
        )


def error_log(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = datetime.utcnow()
        base_headers = dict(request.headers)
        params = request.args.to_dict()

        request_method = request.environ["REQUEST_METHOD"]
        path = request.environ["PATH_INFO"]
        if request_method == 'POST':
            try:
                params.update(dict(request.get_json(force=True) or {}))
            except:
                pass

        try:
            data, status, headers = func(*args, **kwargs)
            end_time = datetime.utcnow()
            create_and_send_logs.delay(start_time, end_time, status, base_headers, params, request_method, path)
            return (data, status, headers)
        except Exception as e:
            traceback.print_exc()
            end_time = datetime.utcnow()
            create_and_send_logs.delay(start_time, end_time, 500, base_headers, params, request_method, path,
                                       traceback.format_exc())
            send_exception_email(e)
            return {}, 500, {}

    return wrapper


class BaseResource(restful.Resource):
    method_decorators = [format_response, error_log, cors, handle_redirects]

    def options(self, *args, **kwargs):
        return {"status": "OPTIONS OK"}
