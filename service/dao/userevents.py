from config.settings import mongo_client

mongo_client = mongo_client
db = mongo_client.userEvents



def store_user_event_into_db(event_structure):
    insertion = db.events.insert_one(event_structure)
    return str(insertion.inserted_id)