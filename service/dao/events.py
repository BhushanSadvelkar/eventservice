from config.settings import mongo_client

mongo_client = mongo_client
db = mongo_client.logs


def store_event_into_db(event_data, event_triggered_date,
                        event_triggered_epoch):

    event_data.update({"triggered_date": event_triggered_date,
                       "triggered_epoch": event_triggered_epoch})
    _id = db.events.insert_one(event_data)
    return _id.inserted_id


def store_error_log_into_db(event_data, event_triggered_date,
                        event_triggered_epoch):

    event_data.update({"triggered_date": event_triggered_date,
                       "triggered_epoch": event_triggered_epoch})
    _id = db.error_logs.insert_one(event_data)
    return _id.inserted_id


def store_log_into_db(event_data, event_triggered_date,
                        event_triggered_epoch):

    event_data.update({"triggered_date": event_triggered_date,
                       "triggered_epoch": event_triggered_epoch})
    _id = db.logs.insert_one(event_data)
    return _id.inserted_id
