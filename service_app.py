import os
import django
from flask import Flask
import flask_restful as restful
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

#SERVICE LAYER IMPORTS
from service.http.events import Events
from service.http.userevent import UserEvent

app = Flask(__name__)
event_api = restful.Api(app, prefix='/eventservice/v1/')


#API END POINTS
event_api.add_resource(Events, 'events/')
event_api.add_resource(UserEvent,'userevent/')


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8091, debug=True)
