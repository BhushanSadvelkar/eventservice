from time import time
import json

from config.settings import mongo_client
from service.utils.email_sender import send_mail

mongo_client = mongo_client
db = mongo_client.logs

ONE_DAY_SEC = 24 * 60 * 60


def get_response_time_mongo_query(start_date_sec, end_date_sec):
    return [{
        "$match": {
            "triggered_epoch": {
                "$gte": start_date_sec,
                "$lte": end_date_sec,
            },
            "event_data.handlerClass": {"$in": [
                "/homedelivery/v1/onlineorderstorelist/",
                "/homedelivery/v1/order/<int:id>",
                "/homedelivery/v1/order/"
            ]}
        }
    },
        {
            "$group": {
                "_id": {
                    "url": "$event_data.handlerClass",
                    "requestMethod": "$event_data.requestMethod"
                },
                "responseTime": {
                    "$push": "$event_data.execution_time_in_mili",
                }
            }
        },
        {'$unwind': '$responseTime'},
        {"$sort": {"responseTime": -1}},
        {
            "$group": {
                "_id": "$_id",
                "responseTime": {
                    "$push": "$responseTime",
                },
                "count": {
                    "$sum": 1
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "url": "$_id.url"
                },
                "stats": {
                    "$push": {
                        "requestMethod": "$_id.requestMethod",
                        "count": "$count",
                        "maxResponseTime": {
                            "$max": "$responseTime"
                        },
                        "minResponse": {
                            "$min": "$responseTime"
                        },
                        "avgResponseTime": {
                            "$avg": "$responseTime"
                        },
                        "p10": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.1, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p20": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.2, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p30": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.3, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p40": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.4, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p50": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.5, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p60": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.6, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p70": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.7, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p80": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.8, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        "p90": {
                            "$arrayElemAt": [
                                "$responseTime", {
                                    "$floor": {
                                        "$multiply": [
                                            0.9, {
                                                "$size": "$responseTime"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    ]


def get_api_stats():
    end_date_sec = int(time())
    start_date_sec = end_date_sec - ONE_DAY_SEC
    query = get_response_time_mongo_query(start_date_sec, end_date_sec)
    api_stats = db.logs.aggregate(query, allowDiskUse=True)
    return api_stats


def get_html_content(api_stat):
    content = ""
    heading = content + "<h3>" + "{}".format(str(api_stat["_id"]["url"]).replace("<", "{").replace(">", "}")) + " " \
              + "</h3>" + content
    table_header = heading + "<table style='width:100%; border:5px solid black'>" + \
                   "<tr>" + \
                   "<th>requestMethod</th>" + \
                   "<th>min</th>" + \
                   "<th>avg</th>" + \
                   "<th>max</th>" + \
                   "<th>p90</th>" + \
                   "<th>p80</th>" + \
                   "<th>p70</th>" + \
                   "<th>p60</th>" + \
                   "<th>p50</th>" + \
                   "<th>p40</th>" + \
                   "<th>p30</th>" + \
                   "<th>p20</th>" + \
                   "<th>p10</th>" + \
                   "</tr>"

    table_content = ""
    for stat in api_stat["stats"]:
        table_content = table_content + \
                        "<tr>" + \
                        "<th>" + str(stat["requestMethod"]) + " ---> " + str(stat["count"]) + "</th>" + \
                        "<th>" + str(stat["minResponse"]) + "</th>" + \
                        "<th>" + str(stat["avgResponseTime"]) + "</th>" + \
                        "<th>" + str(stat["maxResponseTime"]) + "</th>" + \
                        "<th>" + str(stat["p10"]) + "</th>" + \
                        "<th>" + str(stat["p20"]) + "</th>" + \
                        "<th>" + str(stat["p30"]) + "</th>" + \
                        "<th>" + str(stat["p40"]) + "</th>" + \
                        "<th>" + str(stat["p50"]) + "</th>" + \
                        "<th>" + str(stat["p60"]) + "</th>" + \
                        "<th>" + str(stat["p70"]) + "</th>" + \
                        "<th>" + str(stat["p80"]) + "</th>" + \
                        "<th>" + str(stat["p90"]) + "</th>" + \
                        "</tr>"

    table_content = table_header + table_content + "</table>"

    return table_content


def get_content(api_stats):
    html_content = ""
    for api_stat in api_stats:
        html_content = html_content + get_html_content(api_stat)
    return html_content


def send_api_stats_mail():
    api_stats = get_api_stats()
    content = get_content(api_stats)
    send_mail(
        content=json.dumps(content),
        sender_mail="ooops@mpaani.com",
        sender_name="mPaani",
        receiver_list=["bhushan@mpaani.com",
                       "deepak@mpaani.com",
                       "vaibhav@mpaani.com",
                       "shubham@mpaani.com"],
        subject="B2C Web Order Page Stats"
    )
    return "mail send"


print send_api_stats_mail()
