from time import time
import json

from config.settings import mongo_client
from service.utils.email_sender import send_mail

mongo_client = mongo_client
db = mongo_client.logs

ONE_DAY_SEC = 24 * 60 * 60


def get_response_time_mongo_query(start_date_sec, end_date_sec):
    return [
        {
            "$match": {
                "triggered_epoch": {
                    "$gte": start_date_sec,
                    "$lte": end_date_sec,
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "url": "$event_data.handlerClass",
                    "requestMethod": "$event_data.requestMethod",
                    "error": "$event_data.error"
                },
                "responseTime": {
                    "$push": "$event_data.execution_time_in_mili",
                }
            }
        },
        {'$unwind': '$responseTime'},
        {"$sort": {"responseTime": -1}},
        {
            "$group": {
                "_id": "$_id",
                "responseTime": {
                    "$push": "$responseTime",
                },
                "count": {
                    "$sum": 1
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "url": "$_id.url"
                },
                "stats": {
                    "$push": {
                        "requestMethod": "$_id.requestMethod",
                        "count": "$count",
                        "error": "$_id.error"
                    }
                }
            }
        }
    ]


def get_api_stats():
    end_date_sec = int(time())
    start_date_sec = end_date_sec - ONE_DAY_SEC
    query = get_response_time_mongo_query(start_date_sec, end_date_sec)
    api_stats = db.error_logs.aggregate(query)
    return api_stats


def get_html_content(api_stat):
    content = ""
    heading = content + "<h3>" + "{}".format(str(api_stat["_id"]["url"]).replace("<", "{").replace(">", "}")) + " " \
              + "</h3>" + content
    table_header = heading + "<table style='width:100%; border:5px solid black'>" + \
                   "<tr>" + \
                   "<th>requestMethod</th>" + \
                   "<th>error</th>" + \
                   "</tr>"

    table_content = ""
    for stat in api_stat["stats"]:
        table_content = table_content + \
                        "<tr>" + \
                        "<th>" + str(stat["requestMethod"]) + " ---> " + str(stat["count"]) + "</th>" + \
                        "<th>" + str(stat["error"]) + "</th>" + \
                        "</tr>"

    table_content = table_header + table_content + "</table>"

    return table_content


def get_content(api_stats):
    html_content = ""
    for api_stat in api_stats:
        html_content = html_content + get_html_content(api_stat)
    return html_content


def send_api_stats_mail():
    api_stats = get_api_stats()
    content = get_content(api_stats)
    send_mail(
        content=json.dumps(content),
        sender_mail="ooops@mpaani.com",
        sender_name="mPaani",
        receiver_list=["bhushan@mpaani.com",
            "deepak@mpaani.com",
            "shubham@mpaani.com",
            "vaibhav@mpaani.com"],
        subject="Daily Error Stats"
    )
    return "mail send"

print send_api_stats_mail()
