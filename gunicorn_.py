import os

def num_CPUs():
    if not hasattr(os, "sysconf"):
        raise RuntimeError("No sysconf detected.")
    return os.sysconf("SC_NPROCESSORS_ONLN")

bind = "0.0.0.0:8098"
workers = 5
backlog = 512
worker_class = "gevent"
debug = True
daemon = False
pidfile = "/tmp/eventservice-gunicorn.pid"
logfile = "/tmp/eventservice-gunicorn.log"
loglevel = 'info'
accesslog = '/tmp/gunicorn-access.log'
timeout = 50
proc_name = "eventservice"

