import json
import requests


class EventserviceClient():
    base_url = ""

    def __init__(self, base_url):
        self.base_url = base_url

    def post_event(self, event_payload):
        url = self.base_url
        payload = json.dumps(event_payload)
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        return response.json()