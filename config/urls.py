from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import RedirectView
from transactionservice.settings import config

admin.site.site_header = "mPaani Admin"
admin.site.site_title = "mPaani Admin Portal"
admin.site.index_title = "Welcome to mPaani dashboard"
admin.site.site_url = config["CATADMIN_URL"]

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]
