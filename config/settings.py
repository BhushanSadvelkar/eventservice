import os
import pymongo

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'yvj9#k=apq=w-9tnb6qd1tcw+m8!&bro#g2#@!vtn_f!9y@a5q'
DEBUG = True
ALLOWED_HOSTS = ["*"]

config = {}

config["DB"] = {
    "USER": os.environ.get("DB_USER", None),
    "PASSWORD": os.environ.get("DB_PASSWORD", None),
    "HOST": os.environ.get("DB_HOST", None)
}

config['AWS'] = {
    "KEY": os.environ.get('AWS_ACCESS_KEY_ID'),
    "SECRET": os.environ.get('AWS_SECRET_ACCESS_KEY')
}

config["EMAIL_SENDER"] = os.environ.get("EMAIL_SENDER", None)
config["SENDERNAME"] = os.environ.get("SENDERNAME", None)
config["EMAIL_RECIPIENT"] = os.environ.get("EMAIL_RECIPIENT", None)

config["SMTP"] = {
    "USERNAME": os.environ.get("USERNAME_SMTP", None),
    "PASSWORD": os.environ.get("PASSWORD_SMTP", None)
}
config['B2C_DEEPLINK_PREFIX'] = os.environ.get("B2C_DEEPLINK_PREFIX", None)
config["HOST"] = os.environ.get("HOST", None)
config["PORT"] = os.environ.get("PORT", None)

config["PROMOTIONAL"] = {
    "USERNAME": os.environ.get("PROMOTIONAL_USERNAME", None),
    "PASSWORD": os.environ.get("PROMOTIONAL_PASSWORD", None)
}

config["TRANSACTIONAL"] = {
    "USERNAME": os.environ.get("TRANSACTIONAL_USERNAME", None),
    "PASSWORD": os.environ.get("TRANSACTIONAL_PASSWORD", None)
}

config["OTP"] = {
    "USERNAME": os.environ.get("OTP_USERNAME", None),
    "PASSWORD": os.environ.get("OTP_PASSWORD", None)
}

config["SENDER_ID"] = os.environ.get("SENDER_ID", None)
config["KAFKA_BROKER"] = os.environ.get("KAFKA_BROKER", None)

config["B2B_FCM"] = {"PASSWORD": os.environ.get("B2B_FCM_API_KEY", None)}
config["B2C_FCM"] = {"PASSWORD": os.environ.get("B2C_FCM_API_KEY", None)}
config["MPAANI_DJANGO_HOST"] = os.environ.get("MPAANI_DJANGO_HOST", None)
config["ORDER_REVIEW_URL"] = "http://bit.ly/2YPfKfH"

razorpay = {
    "API_KEY": os.environ.get("RAZORPAY_API_KEY", None),
    "PASSWORD": os.environ.get("RAZORPAY_API_SECRET", None)
}

redis = {
    "HOST": os.environ.get("REDIS_HOST", None),
    "PASSWORD": os.environ.get("REDIS_PASS", None),
}


mongo_client = pymongo.MongoClient(
    os.environ.get('MONGODB_HOST'),
    username=os.environ.get('MONGODB_USER'),
    password=os.environ.get('MONGODB_PASSWORD'),
    authSource='logs',
    authMechanism='SCRAM-SHA-256'
)


from redis import Redis

redis_conn = Redis(
    host=redis["HOST"],
    password=redis["PASSWORD"]
)

import boto3

key = config['AWS']['KEY']
secret = config['AWS']['SECRET']
aws_client = boto3.client("s3", region_name="ap-south-1", aws_access_key_id=key, aws_secret_access_key=secret)
config["PLACES_KEY"] = os.environ.get("PLACES_KEY")


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'transactionservice.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'homedelivery.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'transactionservice',
        'USER': config["DB"]["USER"],
        'PASSWORD': config["DB"]["PASSWORD"],
        'HOST': config["DB"]["HOST"]
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'



from clients.service_client import EventserviceClient
config["TRANSACTION_CLIENT"] = EventserviceClient(os.environ["TRANSACTION_BASE_URL"])
